---
title: Route from Myllyoja to Vierumäki and back
description: This was quite randomized trip along roads, sawdust paths and trails. Partially going along the Juustopolku. There are really no options/variations in Myllyoja-area to drive around crisscross here and there, its either roads with some road capable bike or mountainbiking along Juustopolku which doesn't give you options to detour or take shortcuts. The area around Vierumäki lakeside is nice with several paths going along shoreline or on small ridges, and has spots for swimming and resting. Few lean-tos also on the track for resting or chilling out. Yo!
slug: myllyoja-vierumaki
youtubeId: mLs9_bHHqSw
nearestCity: heinola
date: 2019-06-04T17:25:06.022Z
activities:
  - hiking
  - mountainbiking
---
