---
title: Around lake Sorvanen, Lahti (varitation 1)
description: Nice trail route for walking or mountainbiking with some steep downhills and uphills. There are few swimming spots. You might encounter free roaming dogs and some wanderers. Has few difficult spots for mtb beginners, but semi-entertaining and relaxing for advanced mountainbikers.
slug: around-lake-sorvanen-variation-1
youtubeId: xCgdwaJDLQQ
nearestCity: lahti
date: 2019-07-07T17:25:12.654Z
activities:
  - running
  - mountainbiking
---
