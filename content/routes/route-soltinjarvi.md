---
title: Around lake Soltinjärvi, Lahti
description: Partially difficult, slow technical, and swampy trail for mountainbiking, prefer bigger and fatter wheels due roots and swamps, and narrower handlebar due narrow singletracks. Nice for walking. Few unofficial fireplaces on nice locations.
slug: around-lake-soltinjarvi-lahti
youtubeId: 3LIy3ysA_K4
nearestCity: lahti
date: 2019-05-22T20:32:18.921Z
activities:
  - walking
  - running
  - mountainbiking
---
