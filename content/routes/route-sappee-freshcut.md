---
title: Enduro trail in Sappee called "Freshcut"
description: Only little pedaling needed along the way so enjoyable with all sorts of bikes. Few really rocky spots and one very difficult to get through.
slug: enduro-trail-sappee-freshcut
youtubeId: 7mtp7HE_k0Y
cues:
  - seconds: 134
    text: rock garden that is not beginner friendly
  - seconds: 201
    text: really narrow spot
nearestCity: tampere
date: 2019-08-08T17:25:06.022Z
activities:
  - mountainbiking
---
