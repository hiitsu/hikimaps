---
title: One of the tracks in Sappee Bike Park
description: Starts straight from the lift.
slug: sappee-track-1
youtubeId: ECv2ORjQidc
nearestCity: tampere
date: 2019-08-08T17:25:06.022Z
activities:
  - mountainbiking
---
