---
title: Enduro trail in Messilä nicknamed "Masto" - Another variation
description: Another variation of Masto track
slug: enduro-trail-messila-masto-2
youtubeId: 8nuUl6o-pn4
nearestCity: lahti
date: 2019-06-06T17:25:06.022Z
activities:
  - mountainbiking
---
