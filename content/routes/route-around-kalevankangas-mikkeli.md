---
title: Around Kalevankangas
description: Trail circulating around Kalevankangas in Mikkeli
slug: around-kalevankanvas-mikkeli
nearestCity: mikkeli
date: 2019-07-04T16:30:57.386Z
youtubeId: 4gw2lXuTjYw
activities:
  - hiking
  - running
  - mountainbiking
---

**Parking** can be found next to icehockey hall or anywhere along the trail.

You can get a bit lost if goinging northwards but would eventually reach a bigger road so little extra water should be enough for any activity here.

It is mostly quite flat area and walkable trails so a hike even with **children** should be ok. There could be some **geocaches** (unverified though) due the popularity of this area.

This area has alternative trails, if you keep in vicinity of the icehockey hall and suburbs. Trails are mostly easy or moderate for mountainbiking XC, some easy trail riding at most, and just easy for hiking and running. I would not come here to look total peace, solidute and quietness due to nearby residents, but good for few hours of fresh air.
