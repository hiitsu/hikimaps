---
title: Around Jyränkö in Heinola
description: Interesting set of trails going around Jyränkö in Heinola. Lots of waterfront presence, some roads, some easy hiking or biking trails and some impossible ones, that can be circled around. Chances of getting completely lost are almost none. Lots of sharp stones in some parts that can be dangerous in high speeds, for a runner or biker.
slug: around-jyranko-heinola
nearestCity: heinola
youtubeId: TCw9Zlc34KQ
date: 2020-05-17T16:30:57.386Z
activities:
  - hiking
  - mountainbiking
---
