---
title: Jäävuoripolku in Parikkala
description: A trail that goes along ridged formed during the ice age.
slug: jaavuoripolku-parikkala
nearestCity: Parikkala
date: 2019-09-14T16:30:57.386Z
youtubeId: PM3RPprJtmg
activities:
  - hiking
  - mountainbiking
---

The filmed trail starts from the official **Parking** but as you can see from the line, you can get lost pretty easily. And if done well, you'll be in Russia in notime. The cell tower coverage is not super good at all places, so some extra water and energy bar could be useful. Most of the trail was marked and/or noticeable most of the way, but there was moments when it felt like it was vanishing.

There is a nice **lean-to** on the trail. **Nice for mountainbiking or hike**. Maybe not worth travelling here just for MTB or hike, but if lodging in the excellent Papinniemi camping, this could be nice day activity.
