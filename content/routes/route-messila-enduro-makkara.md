---
title: Enduro trail in Messilä nicknamed "Makkara"
description: Nice trail that combines a competion stage with a transition trail in the first half of the video and gps track. Nicknamed "Makkara", in English "Sausage", for unknown reason.
slug: enduro-trail-messila-makkara
youtubeId: ZasZnQtzZFI
nearestCity: lahti
date: 2019-06-06T17:25:06.022Z
activities:
  - mountainbiking
---
