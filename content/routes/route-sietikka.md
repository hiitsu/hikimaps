---
title: Around lake Sietikka, Lahti
description: Nice trail for walking and mountainbiking, which stays mostly in the forest apart from few transitions. You might encounter fox, deer, dogs, hikers or horseriders on this trails, although the wild animals are a rarer sight. No big unforseeable surpises, so good for kids. Semi entertaining for advanced mountainbikers.
slug: around-lake-sietikka-lahti
youtubeId: uqen06K9tsE
cues:
  - seconds: 480
    text: you are entering local motorcycle endure (or quad bike) track
  - seconds: 824
    text: resting place with seats and view to the pond
  - seconds: 862
    text: watch out if somebody is coming to opposite direction!
  - seconds: 932
    text: some kind of old stone circle of seats or something
  - seconds: 1267
    text: possibly deep wet spot, at least the 26" tyre went all the way down
  - seconds: 1855
    text: local bike park with few jumps
nearestCity: lahti
date: 2019-05-18T17:25:06.022Z
activities:
  - walking
  - running
  - mountainbiking
---
