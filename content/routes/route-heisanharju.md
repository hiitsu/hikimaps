---
title: Around Heisanharju recreation area
description: (add description)
slug: around-heisanharju-recreation-area
nearestCity: Kouvola
date: 2019-05-25T16:30:57.386Z
youtubeId: yZnWVeJMDlQ
activities:
  - hiking
  - mountainbiking
---

**Parking** if you come from east GPS system will guide nicely into the official parking, but if you come from west, there is chance that you will get road blocked 1-2km away from the place.

If you dont leave the official area there is little chance of gettin lost as the area is nicely surrounded by recognizable landmarks, mainly lakes of different shapes.

Place is overshadowed by the overpopular Repovesi, and having visited here twice during 2019 while it was good weather and weekend, it was really nice not to see almost anybody, so fingercrossed, this could be for those looking to have some peace and privacy.

This place is good for day trip with kids with possibly some **fishing**, also overnight in **laplander's hut** can be recommended. **Swimming** could be nice during summer. For longer hikes and MTB this can be little too far to be worth it.
