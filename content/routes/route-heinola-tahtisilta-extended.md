---
title: A trail from Heinola's Tahtisilta along highway 4
description: (description)
slug: around-heinola-starting-from-tahtisilta
nearestCity: heinola
youtubeId: _LG37LmRApk
date: 2019-01-21T16:30:57.386Z
activities:
  - hiking
  - mountainbiking
---
