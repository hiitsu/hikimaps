---
title: Enduro trail in Messilä nicknamed "Masto" - Competition version
description: Edited version of Masto for the Sportax Enduro series 2019
slug: enduro-trail-messila-masto-1
youtubeId: -Ug4klHzhnk
nearestCity: lahti
date: 2019-06-06T17:25:06.022Z
activities:
  - mountainbiking
---
