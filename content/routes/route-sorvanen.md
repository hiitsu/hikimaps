---
title: Around lake Sorvanen, Lahti
description: Nice trail route for walking or mountainbiking with some steep downhills and uphills. There are few swimming spots. You might encounter free roaming dogs and some wanderers. Has few difficult spots for mtb beginners, but entertaining and relaxing for advanced mountainbikers.
slug: around-lake-sorvanen-lahti
youtubeId: I19n6ZONmBk
nearestCity: lahti
date: 2019-05-05T17:25:12.654Z
activities:
  - running
  - mountainbiking
---
