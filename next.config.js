const { readFile } = require("fs-extra");
const matter = require("front-matter");
const marked = require("marked");
const glob = require("globby");
const {
  renderSiteMap,
  renderErrorPage,
} = require("./helpers/render-statics.js");
const turf = require("@turf/turf");
const replaceExt = require("replace-ext");

function anomalyDetector(telemetry) {
  telemetry.forEach((entry, index, array) => {
    if (index == 0) return;
    const from = turf.point([array[index - 1][1], array[index - 1][0]]);
    const to = turf.point([array[index][1], array[index][0]]);
    const options = { units: "meters" };
    const distance = turf.distance(from, to, options);
    if (distance > 100) {
      console.log(
        `Distance is greater then 100m at indices ${
          index - 1
        } to ${index}, anomaly?`,

        array[index - 1],
        array[index]
      );
    }
  });
}

async function readEntry(path) {
  const parsedMatter = matter(await readFile(path, "utf8"));
  console.log("parsedMatter", parsedMatter);
  const { body: content, attributes: data } = parsedMatter;
  const telemetry = JSON.parse(
    await readFile(replaceExt(path, ".json", "utf8"))
  );
  const feature = turf.lineString(telemetry.map(([lat, lon]) => [lon, lat]));
  const kilometers = turf.length(feature, { unit: "kilometers" });
  console.log("Read entry", data.title);
  anomalyDetector(telemetry);
  return {
    content: marked(content),
    ...data,
    telemetry,
    kilometers,
  };
}

async function readEntries(globPattern) {
  const paths = await glob(globPattern);
  const entries = await Promise.all(paths.map((path) => readEntry(path)));
  return entries;
}

module.exports = {
  async exportPathMap() {
    const entries = await readEntries(__dirname + "/content/routes/*.md");

    const pages = entries.reduce((acc, entry) => {
      return {
        ...acc,
        [`/${entry.slug}`]: {
          page: "/route",
          query: { entry },
        },
      };
    }, {});

    const routes = {
      ...pages,
      "/": {
        page: "/",
        query: {
          entries: entries.map((entry) => {
            const withoutTelemetry = {
              ...entry,
              telemetry: null,
            };
            return withoutTelemetry;
          }),
        },
      },
      "/changelog": { page: "/changelog" },
      "/faq": { page: "/faq" },
    };
    renderSiteMap(
      Object.entries(routes).reduce((acc, [slug, value]) => {
        let date = new Date().toISOString();
        if (value.query && value.query.entry && value.query.entry.date)
          date = value.query.entry.date;
        return acc.concat([{ slug, date }]);
      }, [])
    );
    renderErrorPage();
    return routes;
  },
};
