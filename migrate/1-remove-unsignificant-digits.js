const fs = require("fs");
const glob = require("glob");
const replaceExt = require("replace-ext");

const indices = {
  lat: 0,
  lon: 1,
  millis: 2,
  alt: 3,
  spd: 4,
};

function roundToOneDecimal(num) {
  return Math.round(num * 10) / 10;
}

(async function () {
  const originalFiles = await new Promise((resolve) => {
    glob(__dirname + "/../content/routes/*.json", {}, (err, files) =>
      resolve(files)
    );
  });
  console.log("Input files:", originalFiles);
  originalFiles
    .map(function readOriginalFile(originalFile) {
      console.log(`Reading file ${originalFile}`);
      return {
        originalFile,
        entries: JSON.parse(fs.readFileSync(originalFile)),
      };
    })
    .map(function removeUnsignificantDecimals({ originalFile, entries }) {
      console.log(`Processing file ${originalFile}`);
      entries.forEach((entry) => {
        entry[indices.millis] = Math.round(entry[indices.millis]);
        if (entry[indices.alt])
          entry[indices.alt] = roundToOneDecimal(entry[indices.alt]);
        if (entry[indices.spd])
          entry[indices.spd] = roundToOneDecimal(entry[indices.spd]);
      });
      return { originalFile, entries };
    })
    .forEach(function rewriteFile({ originalFile, entries }) {
      console.log(`Renaming and rewriting file ${originalFile}`);
      const originalFileRenamed = replaceExt(originalFile, ".json2");
      fs.renameSync(originalFile, originalFileRenamed);
      fs.writeFileSync(originalFile, JSON.stringify(entries), "utf8");
    });
})();
