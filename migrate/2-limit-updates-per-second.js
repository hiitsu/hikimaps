const fs = require("fs");
const glob = require("glob");
const replaceExt = require("replace-ext");

const indices = {
  lat: 0,
  lon: 1,
  millis: 2,
  alt: 3,
  spd: 4,
};

const FIFTEEN_MINUTES = 15 * 1000 * 60;

(async function () {
  const originalFiles = await new Promise((resolve) => {
    glob(__dirname + "/../content/routes/*.json", {}, (err, files) =>
      resolve(files)
    );
  });
  console.log("Input files:", originalFiles);
  originalFiles
    .map(function readOriginalFile(originalFile) {
      console.log(`Reading file ${originalFile}`);
      const entries = JSON.parse(fs.readFileSync(originalFile));
      const totalMillis = entries[entries.length - 1][indices.millis];
      const bestUpdateFrequencyMillis =
        totalMillis > FIFTEEN_MINUTES ? 1000 : 333;
      return {
        originalFile,
        entries,
        bestUpdateFrequencyMillis,
      };
    })
    .map(function reduceTooManyUpdatesPerSecond({
      originalFile,
      entries,
      bestUpdateFrequencyMillis,
    }) {
      console.log(`Processing file ${originalFile}`);
      const reduced = entries.reduce((list, entry, index) => {
        if (index === 0) list.push(entry);
        else if (
          entry[indices.millis] - list[list.length - 1][indices.millis] >
          bestUpdateFrequencyMillis
        ) {
          list.push(entry);
        }
        return list;
      }, []);
      return { originalFile, entries: reduced };
    })
    .forEach(function rewriteFile({ originalFile, entries }) {
      console.log(`Renaming and rewriting file ${originalFile}`);
      const originalFileRenamed = replaceExt(originalFile, ".json2");
      fs.renameSync(originalFile, originalFileRenamed);
      fs.writeFileSync(originalFile, JSON.stringify(entries), "utf8");
    });
})();
