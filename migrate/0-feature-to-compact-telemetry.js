const fs = require("fs");
const glob = require("glob");
//const { _: argumentFiles } = require("minimist")(process.argv.slice(2));
const replaceExt = require("replace-ext");

(async function () {
  const argumentFiles = await new Promise((resolve) => {
    glob("content/routes/*.json", {}, (err, files) => resolve(files));
  });
  console.log("Input files:", argumentFiles);
  argumentFiles
    .map((file) => {
      console.log("reading file", file);
      return { file, feature: JSON.parse(fs.readFileSync(file)) };
    })
    .map(({ file, feature }) => {
      const list = feature.geometry.coordinates.map((lonlat) =>
        lonlat.reverse()
      );
      list.forEach((latlon, index) => {
        const micros = feature.properties.RelativeMicroSec[index];
        const millis = micros / 1000;
        latlon.push(millis);
      });

      return { file, list };
    })
    .forEach(({ file, list }) => {
      //const output = replaceExt(file, ".json2");
      console.log("writing file", file);
      fs.writeFileSync(file, JSON.stringify(list), "utf8");
    });
})();
