const fs = require("fs");
const pug = require("pug");

function renderPug(pugString, fileTo, locals, options) {
  return new Promise(function(resolve, reject) {
    const fn = pug.compile(pugString, options);
    const html = fn(locals);
    fs.writeFileSync(fileTo, html, "utf8");
    resolve(html);
  });
}

const siteMap = `
doctype xml
urlset(xmlns='http://www.sitemaps.org/schemas/sitemap/0.9')
  each page in pages
    url
      loc= page.slug
      lastmod= page.date
      changefreq monthly
      priority 1.0
`;

function renderSiteMap(pages) {
  return renderPug(
    siteMap,
    __dirname + "/../static/sitemap.xml",
    { pages },
    { doctype: "xml" }
  );
}

const errorPage = `
style.
  h1,
  p,
  a {
    font-family: monospace;
    -webkit-font-smoothing: antialiased;
    color: #38b61b;
    letter-spacing: -0.5px;
  }
  h1 {
    font-size: 24px;
  }
  p {
    font-size: 16px;
  }
  a {
    text-decoration: underline;
    font-weight: bold;
  }

  main {
    width: 100%;
    max-width: 640px;
    margin: 0 auto;
    box-sizing: border-box;
  }
  main {
    padding-bottom: 160px;
  }

h1 Page Not Found
p Looks like the page you are looking for does not exist, or its location has changed. Try <a href="/">hikimaps</a> front page.

`;

function renderErrorPage(pages) {
  return renderPug(errorPage, __dirname + "/../static/404.html");
}

module.exports = { renderPug, renderSiteMap, renderErrorPage };
