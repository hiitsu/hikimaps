/**
 * Creating a page named _error.js lets you override HTTP error messages
 */
import React from "react";
import { withRouter } from "next/router";

class ErrorPage extends React.Component {
  static propTypes() {
    return {
      errorCode: React.PropTypes.number.isRequired,
      url: React.PropTypes.string.isRequired
    };
  }

  static getInitialProps({ res, xhr }) {
    const errorCode = res ? res.statusCode : xhr ? xhr.status : null;
    return { errorCode };
  }

  renderMessage() {
    switch (this.props.errorCode) {
      case 200: // Also display a 404 if someone requests /_error explicitly
      case 404:
        return (
          <>
            <h1>Page Not Found</h1>
            <p>
              Looks like the page you are looking for does not exist, or its
              location has changed. Try <a href="/">hikimaps</a> front page.
            </p>
          </>
        );

      case 500:
      default:
        return (
          <>
            <h1>Internal Server Error</h1>
            <p>An internal server error occurred.</p>
          </>
        );
    }
  }
  render() {
    return (
      <main>
        <style global jsx>{`
          h1,
          p,
          a {
            font-family: monospace;
            -webkit-font-smoothing: antialiased;
            color: #38b61b;
            letter-spacing: -0.5px;
          }
          h1 {
            font-size: 24px;
          }
          p {
            font-size: 16px;
          }
          a {
            text-decoration: underline;
            font-weight: bold;
          }

          main {
            width: 100%;
            max-width: 640px;
            margin: 0 auto;
            box-sizing: border-box;
          }
          main {
            padding-bottom: 160px;
          }
        `}</style>
        {this.renderMessage()}
      </main>
    );
  }
}

export default withRouter(ErrorPage);
