import React, { Component, StrictMode } from "react";

import Head from "next/head";

import Layout from "../components/Layout";
import Distance from "../components/Distance";
import Icon from "../components/Icon";
import LazyMap from "../components/LazyMap";
import PlayerHtml5Video from "../components/PlayerHtml5Video";
import PlayerYoutubeLazy from "../components/PlayerYoutubeLazy";
import Speed from "../components/Speed";
import Elevation from "../components/Elevation";
import ElevationChart from "../components/ElevationChart";
import TelemetryHelper from "../components/TelemetryHelper";
import * as gtag from "../components/gtag";

export default class PageRoute extends Component {
  static async getInitialProps({ query: { entry } }) {
    return { entry };
  }

  constructor(props) {
    super(props);

    this.handleCueClick = this.handleCueClick.bind(this);
    this.handleMapPolylineClick = this.handleMapPolylineClick.bind(this);
    this.handleMapViewportChange = this.handleMapViewportChange.bind(this);
    this.handleMapClick = this.handleMapClick.bind(this);

    this.handleElevationChartClick = this.handleElevationChartClick.bind(this);

    this.handleVideoTick = this.handleVideoTick.bind(this);
    this.handleVideoPlayOrJumpToByUser = this.handleVideoPlayOrJumpToByUser.bind(
      this
    );

    const telemetry = this.props.entry.telemetry;
    const hasSpeed = TelemetryHelper.hasSpeed(telemetry);
    const hasElevation = TelemetryHelper.hasElevation(telemetry);
    this.state = {
      telemetry,
      hasSpeed,
      hasElevation,
      speed: hasSpeed ? TelemetryHelper.speedAt(telemetry, 0) : 0,
      elevation: hasElevation ? TelemetryHelper.elevationAt(telemetry, 0) : 0,
      videoMillis: 0,
    };
  }

  componentDidMount() { }

  handleElevationChartClick(jumpToSeconds) {
    gtag.event({
      action: "ElevationChartClick",
      category: "Video",
      label: this.props.entry.title,
    });
    this.setState({ jumpToSeconds });
  }

  handleCueClick(jumpToSeconds) {
    gtag.event({
      action: "CueClick",
      category: "Video",
      label: this.props.entry.title,
    });
    this.setState({ jumpToSeconds });
  }

  handleMapPolylineClick(jumpToSeconds) {
    gtag.event({
      action: "MapPolylineClick",
      category: "Map",
      label: this.props.entry.title,
    });
    this.setState({ jumpToSeconds });
  }

  handleMapViewportChange({ zoom }) {
    gtag.event({
      action: "MapViewportChange",
      category: "Map",
      label: this.props.entry.title,
      value: zoom,
    });
  }

  handleMapClick() {
    gtag.event({
      action: "MapClick",
      category: "Map",
      label: this.props.entry.title,
    });
  }

  handleVideoPlayOrJumpToByUser() {
    gtag.event({
      action: "VideoPlayOrJump",
      category: "Video",
      label: this.props.entry.title,
    });
  }

  handleVideoTick(seconds) {
    //console.log("handleSyncMarker", seconds);
    const videoMillis = seconds * 1000;
    const marker = TelemetryHelper.latLonAt(this.state.telemetry, videoMillis);
    const speed = TelemetryHelper.speedAt(this.state.telemetry, videoMillis);
    const elevation = TelemetryHelper.elevationAt(
      this.state.telemetry,
      videoMillis
    );
    this.setState({ marker, videoMillis, speed, elevation });
  }

  render() {
    const {
      content: html,
      title,
      description,
      slug,
      youtubeId,
      awsBucketUrl,
      cues,
      activities,
      telemetry,
      kilometers,
    } = this.props.entry;
    const {
      marker,
      jumpToSeconds,
      hasSpeed,
      hasElevation,
      speed,
      elevation,
      videoMillis,
    } = this.state;
    const hasVideo = (youtubeId || awsBucketUrl);
    return (
      <StrictMode>
        <Head>
          <title>{title}</title>
          <meta key="description" content={description} />
          <meta key="og:title" content={title} />
          <meta key="og:description" content={description} />
          <meta key="og:url" content={"https://www.hikimaps.com/" + slug} />
          <meta key="twitter:title" content={title} />
          <meta key="twitter:description" content={description} />
        </Head>
        <Layout>
          <article>
            <h1>
              {title}
              {activities &&
                activities.map((activity, index) => (
                  <Icon key={index} figure={activity} className="route" />
                ))}
              <Distance
                kilometers={kilometers}
                style={{
                  margin: "0 1em",
                  width: "3em",
                  display: "inline-block",
                }}
              />
            </h1>
            {html ? (
              <div
                className="content"
                dangerouslySetInnerHTML={{ __html: html }}
              />
            ) : (
                <p>(no text)</p>
              )}
            {hasVideo && (
              <p
                style={{
                  paddingLeft: "2em",
                  position: "relative",
                  paddingBottom: "0.8em",
                }}
              >
                <Icon
                  figure="marker"
                  style={{ position: "absolute", left: 0, top: -2 }}
                />{" "}
                Video position on the line
              </p>
            )}
            <LazyMap
              marker={marker}
              telemetry={telemetry}
              onMapClick={this.handleMapClick}
              onMapViewportChange={this.handleMapViewportChange}
              onClickPolyline={this.handleMapPolylineClick}
            />
            {youtubeId && (
              <PlayerYoutubeLazy
                videoId={youtubeId}
                onTick={this.handleVideoTick}
                jumpToSeconds={jumpToSeconds}
                onVideoPlayOrJumpToByUser={this.handleVideoPlayOrJumpToByUser}
              />
            )}
            {awsBucketUrl && (
              <PlayerHtml5Video
                src={awsBucketUrl}
                onTick={this.handleVideoTick}
                jumpToSeconds={jumpToSeconds}
                onVideoPlayOrJumpToByUser={this.handleVideoPlayOrJumpToByUser}
              />
            )}

            {hasVideo && (hasElevation || hasSpeed) && (
              <section className="stats">
                {hasSpeed && (
                  <Speed
                    style={{ position: "absolute", top: "0.3em", left: "6em" }}
                    ms={speed}
                  />
                )}
                {hasElevation && (
                  <Elevation
                    style={{ position: "absolute", top: "0.3em", left: "0" }}
                    m={elevation}
                  />
                )}
              </section>
            )}
            {hasElevation && (
              <ElevationChart
                telemetry={telemetry}
                videoMillis={videoMillis}
                onClick={this.handleElevationChartClick}
              />
            )}
            {!hasElevation && (
              <p className="no-elevation-data-notice">
                <em>!</em>This route does not have elevation data available
              </p>
            )}

            {cues && (
              <>
                <h2>In the video:</h2>
                <ul className="cues">
                  {cues.map(({ seconds, text }, index) => {
                    return (
                      <li key={index}>
                        <a
                          className="cue-link"
                          onClick={() => this.handleCueClick(seconds)}
                        >
                          {text}
                          <Icon
                            style={{
                              display: "inline-block",
                              paddingTop: 4,
                              paddingLeft: 4,
                            }}
                            figure="play"
                          />
                        </a>
                      </li>
                    );
                  })}
                </ul>
              </>
            )}

            <style jsx>{`
              h1,
              h2,
              h3,
              h4,
              h5,
              p,
              label,
              dd,
              dt,
              .content,
              a {
                color: #1bb653;
              }
              article {
                width: 100%;
                max-width: 640px;
                margin: 0 auto;
                box-sizing: border-box;
                padding-bottom: 140px;
              }
              article > h1 {
                margin: 0;
                letter-spacing: 1px;
                padding-top: 1.1rem;
                padding-bottom: 1.1rem;
              }
              article > p {
                margin: 0;
                padding-bottom: 1.4rem;
                font-size: 1.2em;
              }
              article > p.no-elevation-data-notice {
                margin-top: 1em;
                box-sizing: border-box;
                border: 1px solid #1bb653;
                padding: 2em 1em 2em 4em;
                position: relative;
              }
              .stats {
                height: 5em;
                position: relative;
              }
              ul.cues {
                color: #1bb653;
              }
              a.cue-link:hover {
                text-decoration: underline;
              }
              @media screen and (max-width: 645px) {
                article > h1,
                article > p,
                article > .content {
                  margin: 0 8px;
                }
              }
              em {
                border-radius: 50%;
                margin-right: 0.5em;
                border: 1px solid #1bb653;
                width: 2em;
                height: 2em;
                line-height: 2em;
                display: block;
                font-size: 0.8em;
                text-align: center;
                box-sizing: border-box;
                position: absolute;
                left: 2em;
                top: calc(50% - 1em);
              }
            `}</style>
          </article>
        </Layout>
      </StrictMode>
    );
  }
}
