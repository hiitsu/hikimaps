import React, { Component, StrictMode } from "react";

import Head from "next/head";

import Layout from "../components/Layout";
import RouteListImageLink from "../components/RouteListImageLink";
import RouteListTextLink from "../components/RouteListTextLink";

export default class PageIndex extends Component {
  static async getInitialProps({ query: { entries } }) {
    return { entries };
  }

  constructor(props) {
    super(props);

    const locations = [
      ...new Set(props.entries.map((entry) => entry.nearestCity)),
    ];
    this.state = {
      locations,
    };
  }

  render() {
    const { entries } = this.props;
    const description =
      "List of possibly interesting outdoor routes around Finland for walking, hiking, mountainbiking and such";
    const title = "Hikimaps - Routes for active people";
    return (
      <StrictMode>
        <Head>
          <title>{title}</title>
          <meta key="description" content={description} />
          <meta key="og:title" content={title} />
          <meta key="og:description" content={description} />
          <meta key="og:url" content={"https://www.hikimaps.com/"} />
          <meta key="twitter:title" content={title} />
          <meta key="twitter:description" content={description} />
        </Head>

        <Layout>
          <main role="main">
            <h1>Locations</h1>
            {this.state.locations.map((location, index) => {
              return (
                <section key={index}>
                  <h2>{location}</h2>
                  <ul>
                    {entries
                      .filter((entry) => entry.nearestCity == location)
                      .map((entry, index) => (
                        <RouteListTextLink key={index} entry={entry} />
                      ))}
                  </ul>
                </section>
              );
            })}
          </main>
        </Layout>
        <style jsx>{`
          main {
            width: 100%;
            max-width: 640px;
            margin: 0 auto;
            box-sizing: border-box;
          }
          main {
            padding-bottom: 160px;
          }
          h1 {
            color: #38b61b;
            letter-spacing: 0;
            padding-top: 2em;
            border-bottom: 1px dashed #38b61b;
            padding-bottom: 0.4em;
            margin-bottom: 2em;
            font-size: 1.8em;
            font-weight: 500;
          }
          section {
            margin-bottom: 3em;
          }
          h2 {
            font-weight: normal;
            text-transform: capitalize;
            margin-bottom: 1em;
            color: #38b61b;
            letter-spacing: 0;
          }
          ul {
            list-style-type: circle;
            list-style: none;
            padding-inline-start: 0;
            display: inline-block;
            width: 100%;
          }
          @media screen and (max-width: 680px) {
            main {
              padding: 0 8px;
            }
          }
          @media screen and (max-width: 480px) {
            h1,
            h2 {
              text-align: center;
            }

            ul {
              padding-inline-start: 0;
            }
          }
        `}</style>
      </StrictMode>
    );
  }
}
