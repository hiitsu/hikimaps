import React, { Component, StrictMode } from "react";

import Head from "next/head";

import Layout from "../components/Layout";

export default class PageFAQ extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const description =
      "Frequenty asked questions regarding the site, its development or related things.";
    const title = "Hikimaps - FAQ";
    return (
      <StrictMode>
        <Head>
          <title>{title}</title>
          <meta key="description" content={description} />
          <meta key="og:title" content={title} />
          <meta key="og:description" content={description} />
          <meta key="og:url" content={"https://www.hikimaps.com/"} />
          <meta key="twitter:title" content={title} />
          <meta key="twitter:description" content={description} />
        </Head>

        <Layout>
          <main role="main">
            <h1>Frequenty asked questions</h1>

            <dl class="faq">
              <dt>
                Why does hikimaps exist when there are so many route, video and
                outdoor sites already?
              </dt>
              <dd>
                <p>
                  It is a pet project that encourages to go to new places and
                  keep condition up. Author is a programmer no-lifer who needs a
                  hobby.
                </p>
              </dd>

              <dt>Why is speed jumping back and forth?</dt>
              <dd>Seem to be just GoPro calculating average multiple times per second,
              and with web friendly exported and reduced dataset, a compromise was made not
                to try to recalculate and normalize the speed trends to avoid further inaccuracies.</dd>

              <dt>Why is video and map marker off sync sometimes?</dt>
              <dd>
                GoPro seems to lose time sometimes or save invalid value (a
                glitch) and then the metadata importing tools just fail. So it
                might take a while, before it is noticed and fixed sense to
                whole import, export upload procedure is semi-automated, so it
                may get fixed when it gets fixed.
              </dd>
            </dl>
          </main>
        </Layout>
        <style jsx>{`
          main {
            width: 100%;
            max-width: 640px;
            margin: 0 auto;
            box-sizing: border-box;
          }
          main {
            padding-bottom: 160px;
          }
          h1 {
            color: #38b61b;
            letter-spacing: 0;
            padding-top: 2em;
            border-bottom: 1px dashed #38b61b;
            padding-bottom: 0.8em;
            margin-bottom: 2em;
            font-size: 1.8em;
            font-weight: 500;
          }
          dl,
          dt,
          dd {
            color: #38b61b;
            margin-bottom: 3.5em;
          }
          dt {
            margin-bottom: 1.5em;
            font-weight: bold;
          }

          dd {
            font-style: normal;
          }
          p {
            margin: 0 0 0.5em 0;
          }
          @media screen and (max-width: 645px) {
            main {
              padding-left: 10px;
              padding-right: 10px;
            }
          }
        `}</style>
      </StrictMode>
    );
  }
}
