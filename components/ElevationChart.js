import React, { Component } from "react";
import TelemetryHelper, { indices } from "./TelemetryHelper";

function range(n) {
  return Array(n)
    .fill()
    .map((_, i) => i);
}

export default class ElevationChart extends Component {
  constructor(props) {
    super(props);

    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.handleMouseOut = this.handleMouseOut.bind(this);
    this.handleClick = this.handleClick.bind(this);

    const values = TelemetryHelper.timeSeriesAltitude(this.props.telemetry);
    const stepY = 20;
    const altitudeValues = values.map((a) => a[1]).filter(Number.isFinite);
    const maxAltitude = altitudeValues.reduce(
      (max, current) => (current > max ? current : max),
      Number.MIN_VALUE
    );
    const minAltitude = altitudeValues.reduce(
      (min, current) => (min < current ? min : current),
      Number.MAX_VALUE
    );
    const maxY = Math.ceil(maxAltitude / stepY) * stepY;
    const minY = Math.floor(minAltitude / stepY) * stepY;
    const maxX = values[values.length - 1][0];

    const totalMillis = values[values.length - 1][0];

    function altitudeAt(x) {
      const xMillis = (x / 640) * totalMillis;
      const entry = values.find((entry, index) => {
        if (index >= values.length - 1) return true;
        return entry[0] >= xMillis;
      });
      return entry[1];
    }
    const points = range(640)
      .map((x) => [
        x,
        240 - Math.round(((altitudeAt(x) - minY) / (maxY - minY)) * 240),
      ])
      .map((a) => a.join(","))
      .join(" ");

    const steps = Array((maxY - minY) / stepY + 1)
      .fill()
      .map((_, index) => minY + index * stepY);

    this.state = {
      x: 0,
      points,
      maxY,
      maxX,
      minY,
      stepY,
      steps,
      cursorX: 320,
      videoX: 0,
      isMouseIn: false,
    };
  }

  componentDidMount() {}

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.videoMillis !== this.props.videoMillis) {
      const videoX = (this.props.videoMillis / this.state.maxX) * 640;
      this.setState({ videoX });
    }
  }

  handleClick(ev) {
    const rect = ev.currentTarget.getBoundingClientRect();
    const cursorX = ev.pageX - rect.left;
    const offsetMillis = this.state.maxX * (cursorX / 640);
    const offsetSeconds = parseInt(offsetMillis / 1000);
    this.props.onClick && this.props.onClick(offsetSeconds);
  }

  handleMouseMove(ev) {
    const rect = ev.currentTarget.getBoundingClientRect();
    const cursorX = ev.pageX - rect.left;
    this.setState({ cursorX, isMouseIn: true });
  }

  handleMouseOut() {
    this.setState({ isMouseIn: false });
  }

  render() {
    const {
      isMouseIn,
      cursorX,
      points,
      videoX,
      minY,
      maxY,
      steps,
    } = this.state;
    return (
      <figure>
        <svg
          onMouseMove={this.handleMouseMove}
          onMouseOut={this.handleMouseOut}
          onClick={this.handleClick}
          xmlns="http://www.w3.org/2000/svg"
          version="1.1"
          width="100%"
          viewBox="0 0 640 240"
          preserveAspectRatio="none"
        >
          {steps.map((meters, index) => {
            const y = 240 - ((meters - minY) / (maxY - minY)) * 240;
            return (
              <g key={index} className="y">
                <text x="-5" y={y + 5}>
                  {meters}
                </text>
                <line x1="0" x2="640" y1={y} y2={y} />
              </g>
            );
          })}
          <polyline points={points} />
          {isMouseIn && (
            <line
              className="cursor"
              x1={cursorX}
              x2={cursorX}
              y1={0}
              y2={240}
            />
          )}
          <line className="video" x1={videoX} x2={videoX} y1={0} y2={240} />
        </svg>
        <style jsx>{`
          figure {
            padding: 0;
            margin: 1em 0 0 0;
            padding: 0 0 0 0;
            box-sizing: border-box;
          }
          svg {
            background: #eee;
            border-left: 1px dotted #777;
            border-bottom: 1px dotted #777;
            box-sizing: border-box;
            margin: 0;
            overflow: visible;
          }
          polyline {
            fill: none;
            stroke: #1bb654;
            stroke-width: 1;
          }
          text {
            text-anchor: end;
            padding-left: 1em;
            fill: #333;
          }
          line {
            stroke: #ccc;
            stroke-style: dotted;
            stroke-width: 1;
          }
          .cursor {
            stroke: #333;
            stroke-width: 2;
          }
          .video {
            stroke: #1bb654;
            stroke-width: 2;
          }
        `}</style>
      </figure>
    );
  }
}
