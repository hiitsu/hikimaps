function Speed({ style, ms }) {
  let kmh = Math.ceil(ms * 3.6 * 10) / 10;
  if (kmh < 1.5) kmh = 0;
  return (
    <span style={style} className="speed">
      <span className="value">{kmh}</span>
      <span className="unit">km/h</span>
      <style jsx>{`
        .speed {
          background: #fff;
          color: #1bb653;
          display: inline-block;
          border-radius: 50%;
          box-sizing: border-box;
          padding: 0.5em;
          width: 5em;
          height: 5em;
          border: 3px solid #1bb653;
          position: relative;
        }
        .value {
          text-align: center;
          font-size: 2em;
          width: 100%;
          display: inline-block;
          letter-spacing: -4px;
        }
        .unit {
          position: absolute;
          bottom: 1em;
          left: 10%;
          width: 90%;
          text-align: center;
        }
      `}</style>
    </span>
  );
}
/*
    position: fixed;
    top: 1em;
    left: 1em;
    user-select: none;
    z-index: 1110001;
*/
export default Speed;
