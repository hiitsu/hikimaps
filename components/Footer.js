import Link from "next/link";
import Icon from "./Icon";

export default function() {
  return (
    <footer>
      <div className="footer-content">
        <div className="column-left">
          <p>
            <em>Hikimaps</em>
            <br />
          </p>
        </div>
        <div className="column-center">
          <h2>Resources</h2>
          <ul>
            <li>
              <a href="/faq">FAQ</a>
            </li>
            <li>Icons</li>
            <li>
              <a href="/changelog">Change Log</a>
            </li>
            <li>Contact</li>
          </ul>
        </div>
        <div className="column-right">
          <h2>Social</h2>
          <ul>
            <li>(not very social yet)</li>
          </ul>
        </div>
      </div>
      <style jsx>{`
        footer {
          width: 100%;
          padding: 4em 0 16em 0;
          background: #1bb654;
          color: white;
        }
        .footer-content {
          width: 100%;
          max-width: 640px;
          margin: 0 auto;
          box-sizing: border-box;
          height: 4em;
          position: relative;
        }
        .column-left {
          float: left;
          width: 33.333%;
        }
        .column-right {
          float: right;
          width: 33.333%;
        }
        .column-center {
          display: inline-block;
          width: 33.333%;
        }
        em {
          font-style: normal;
          font-size: 1.8em;
        }
        p,
        h2 {
          margin-top: 0;
        }
        h2 {
          border-bottom: 1px dashed rgba(255, 255, 255, 0.8);
          font-weight: normal;
          box-sizing: border-box;
          padding-bottom: 0.5em;
          margin: 0 28px 0 0;
        }
        ul {
          color: rgba(255, 255, 255, 0.8);
          list-style: none;
          padding-inline-start: 0;
        }
        a {
          display: block;
        }
        li,
        a,
        a:visited {
          line-height: 2em;
          height: 2em;
          color: #fff;
          text-decoration: none;
        }
        a:active,
        a:hover {
          font-weight: bold;
          text-decoration: underline;
        }
        @media screen and (max-width: 480px) {
          .footer-content {
            padding-left: 10px;
            padding-right: 10px;
          }
          .column-left {
            width: 100%;
            text-align: center;
            padding-bottom: 2em;
          }
          .column-center,
          .column-right {
            width: 50%;
          }
        }
      `}</style>
    </footer>
  );
}
