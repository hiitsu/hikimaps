import React, { Component } from "react";


export default class PlayerYoutubeLazy extends Component {
    constructor(props) {
        super(props);

        this.handleVideoReady = this.handleVideoReady.bind(this);
        this.handleVideoStateChange = this.handleVideoStateChange.bind(this);
        this.handleVideoPlay = this.handleVideoPlay.bind(this);
        this.handleVideoPause = this.handleVideoPause.bind(this);
        this.handleVideoTick = this.handleVideoTick.bind(this);

    }

    componentDidUpdate(prevProps) {
        if (this.props.jumpToSeconds != prevProps.jumpToSeconds) {
            this.setState({ loadPlayer: true });
            if (this.player) {
                this.player.currentTime = this.props.jumpToSeconds;
            }
        }
    }

    handleVideoTick() {
        if (this.player) {
            const seconds = this.player.currentTime;
            this.props.onTick && this.props.onTick(seconds);
        }
    }

    handleVideoPlay(ev) {
        console.log("handleVideoPlay", ev);
        this.intervalId = setInterval(this.handleVideoTick, 300);
        this.props.onVideoPlayOrJumpToByUser &&
            this.props.onVideoPlayOrJumpToByUser();
    }

    handleVideoPause(ev) {
        console.log("handleVideoPause", ev);
        clearInterval(this.intervalId);
    }

    handleVideoStateChange(ev) {
        console.log("handleVideoStateChange", ev);
    }

    handleVideoReady(ev) {
        this.player = ev.target;
    }

    render() {
        return (
            <video
                width={640}
                controls
                src={this.props.src}
                onCanPlay={this.handleVideoReady}
                onPlay={this.handleVideoPlay}
                onPause={this.handleVideoPause}
            />
        );
    }
}
