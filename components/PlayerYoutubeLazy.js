import React, { Component } from "react";
import YouTube from "react-youtube";

const youtubeVideoOptions = {
  height: "390",
  width: "640",
  playerVars: {
    autoplay: 1
  }
};

export default class PlayerYoutubeLazy extends Component {
  constructor(props) {
    super(props);

    this.handleVideoReady = this.handleVideoReady.bind(this);
    this.handleVideoStateChange = this.handleVideoStateChange.bind(this);
    this.handleVideoPlay = this.handleVideoPlay.bind(this);
    this.handleVideoPause = this.handleVideoPause.bind(this);
    this.handleVideoTick = this.handleVideoTick.bind(this);

    this.handleLoadPlayer = this.handleLoadPlayer.bind(this);

    this.state = {
      loadPlayer: false
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.jumpToSeconds != prevProps.jumpToSeconds) {
      this.setState({ loadPlayer: true });
      if (this.player) {
        this.player.seekTo(this.props.jumpToSeconds, true);
      }
    }
  }

  handleVideoTick() {
    if (this.player) {
      const seconds = this.player.getCurrentTime();
      this.props.onTick && this.props.onTick(seconds);
    }
  }

  handleVideoPlay(ev) {
    console.log("handleVideoPlay", ev);
    this.intervalId = setInterval(this.handleVideoTick, 300);
    this.props.onVideoPlayOrJumpToByUser &&
      this.props.onVideoPlayOrJumpToByUser();
  }

  handleVideoPause(ev) {
    console.log("handleVideoPause", ev);
    clearInterval(this.intervalId);
  }

  handleVideoStateChange(ev) {
    console.log("handleVideoStateChange", ev);
  }

  handleVideoReady(ev) {
    console.log("handleVideoReady", ev);
    this.player = ev.target;
    if (this.props.jumpToSeconds)
      this.player.seekTo(this.props.jumpToSeconds, true);
  }

  handleLoadPlayer() {
    this.setState({ loadPlayer: true });
  }

  render() {
    if (!this.state.loadPlayer) {
      return (
        <div className="video-thumbnail-container">
          <img src={`https://img.youtube.com/vi/${this.props.videoId}/0.jpg`} />
          <button className="play" onClick={this.handleLoadPlayer} />
          <style jsx>{`
            .play {
              background: #282828;
              border-radius: 50% / 10%;
              color: #fff;
              font-size: 1em;
              height: 3em;
              padding: 0;
              text-align: center;
              text-indent: 0.1em;
              transition: all 150ms ease-out;
              width: 4em;
              position: absolute !important;
              top: 50%;
              left: 50%;
              transform: translateX(-50%) translateY(-50%);
              border: none;
              opacity: 0.8;
              cursor: pointer;
              z-index: 9;
            }
            .play:hover {
              background: #ff0000;
            }
            .play::before {
              background: inherit;
              border-radius: 5% / 50%;
              bottom: 9%;
              content: "";
              left: -5%;
              position: absolute;
              right: -5%;
              top: 9%;
            }
            .play:after {
              border-style: solid;
              border-width: 1em 0 1em 1.732em;
              border-color: transparent transparent transparent
                rgba(255, 255, 255, 0.75);
              content: " ";
              font-size: 0.75em;
              height: 0;
              margin: -1em 0 0 -0.75em;
              top: 50%;
              position: absolute;
              width: 0;
            }
          `}</style>
        </div>
      );
    }
    return (
      <YouTube
        containerClassName="youtube-iframe-container"
        videoId={this.props.videoId}
        opts={youtubeVideoOptions}
        onReady={this.handleVideoReady}
        onStateChange={this.handleVideoStateChange}
        onPlay={this.handleVideoPlay}
        onPause={this.handleVideoPause}
      />
    );
  }
}
