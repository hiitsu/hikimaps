import Icon from "../components/Icon";
import Distance from "../components/Distance";

export default function ({ entry }) {
  return (
    <dl>
      {entry.kilometers && (
        <>
          <dt>Distance</dt>
          <dd>
            <Distance kilometers={entry.kilometers} />
          </dd>
        </>
      )}
      <dt>Terrain types</dt>
      <dd>Forest 90%, Roads 10%</dd>
      <dt>Verified</dt>
      <dd>2019</dd>
      <dt>Suitable for</dt>
      <dd>Kids, Dogs</dd>
      <dt>Also possible to</dt>
      <dd>
        {" "}
        <Icon figure="swimming" />
        <Icon figure="walking" />
        <Icon figure="bicycling" />
      </dd>
    </dl>
  );
}
