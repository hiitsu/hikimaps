import React from "react";
import Cookies from "universal-cookie";

const cookies = new Cookies();
const cookieName = "onboarding-polyline-click";

class OnboardingPolylineClick extends React.Component {
  constructor(props) {
    super(props);

    this.handleGotIt = this.handleGotIt.bind(this);

    this.state = {
      show: false
    };
  }

  componentDidMount() {
    const show = !cookies.get(cookieName);
    this.setState({ show });
  }

  handleGotIt() {
    this.setState({ show: false });
    cookies.set(cookieName, "1", { path: "/" });
  }

  render() {
    if (!this.state.show) return null;

    return (
      <div className="OnboardingPolylineClick">
        <p>
          You can click anywhere on the route line and video will synchronize,
          or by forwarding the video should synchronize the marker along the
          route. <button onClick={this.handleGotIt}>Got it</button>
        </p>

        <style jsx>{`
          .OnboardingPolylineClick {
            border-radius: 10px;
            border: 1px solid #38b61b;
            padding: 0 16px;
            position: absolute;
            z-index: 500;
            background: white;
            width: 30%;
            right: 5px;
            top: 5px;
          }
          button {
            border-radius: 5px;
            padding: 6px 14px;
          }
        `}</style>
      </div>
    );
  }
}

export default OnboardingPolylineClick;
