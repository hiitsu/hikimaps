
function Speed({ style, m }) {
  return <span style={style} className="elevation">
    <span className="value">{parseInt(m)}</span>
    <span className="unit">meters</span>
    <style jsx>{`
        .elevation {
          background:#fff;
          color:#1bb653;
          display:inline-block;
          border-radius:50%;
          box-sizing:border-box;
          padding:0.5em;
          width:5em;
          height:5em;
          border: 3px solid #1bb653;
          position:relative;
        }
        .value {
          text-align: center;
          font-size: 1.7em;
          width: 100%;
          display: inline-block;
          letter-spacing: -2px;
        }
        .unit{
          position:absolute;
          bottom:1em;
          left: 10%;
          width: 90%;
          text-align:center;
        }
      `}</style>
  </span>;
}

export default Speed;
