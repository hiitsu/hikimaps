import React, { Component } from "react";
import Icon from "./Icon";
import Distance from "./Distance";

export default function RouteListTextLink({ entry }) {
  return (
    <li>
      <a href={`/${entry.slug}`}>
        <span className="text">{entry.title}</span>
        <Distance
          kilometers={entry.kilometers}
          style={{
            margin: "0 0 0 0.5em",
            fontSize: "0.85em",
          }}
        />
        <span className="icons-wrapper">
          {entry.activities &&
            entry.activities.map((activity, index) => (
              <Icon
                key={`icon-${index}`}
                className="with-border"
                figure={activity}
                // className={`icon-${index}`}
              />
            ))}
        </span>
      </a>
      <style jsx>{`
        li {
          margin-bottom: 0.5em;
          line-height: 1.5em;
        }
        a,
        a:visited,
        a:active {
          display: block;
          line-height: 1.3em;
          color: #38b61b;
          text-decoration: none;
          position: relative;
          padding-right: 7em;
        }
        a:hover {
          text-decoration: underline;
        }
        .icons-wrapper {
          position: absolute;
          right: 0;
          top: 0;
        }

        @media screen and (max-width: 480px) {
          a {
            margin-bottom: 2em;
          }
        }
      `}</style>
    </li>
  );
}
