function humanizeMeters(meters) {
  if (meters >= 10000) return Math.round(meters / 1000) + "km";
  if (meters >= 1000) return (meters / 1000.0).toFixed(1) + "km";
  return Math.round(meters) + "m";
}

function Distance({ kilometers, style }) {
  return <span style={style}>({humanizeMeters(kilometers * 1000)})</span>;
}

export default Distance;
