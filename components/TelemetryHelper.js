export const indices = {
  lat: 0,
  lon: 1,
  millis: 2,
  alt: 3,
  spd: 4,
};

export default class TelemetryHelper {
  static hasElevation(entries) {
    return entries.some((entry) => entry[indices.alt] > 0);
  }
  static hasAltitude(entries) {
    return entries.some((entry) => entry[indices.alt] > 0);
  }
  static hasSpeed(entries) {
    return entries.some((entry) => entry[indices.spd] > 0);
  }

  static findIndex(entries, millis) {
    let index = entries.findIndex((entry) => entry[indices.millis] >= millis);
    if (index === -1 || index > entries.length - 1) index = entries.length - 1;
    return index;
  }

  static speedAt(entries, millis) {
    const index = millis <= 0 ? 0 : TelemetryHelper.findIndex(entries, millis);
    return entries[index][indices.spd];
  }

  static elevationAt(entries, millis) {
    const index = millis <= 0 ? 0 : TelemetryHelper.findIndex(entries, millis);
    return entries[index][indices.alt];
  }

  static altitudeAt(entries, millis) {
    const index = millis <= 0 ? 0 : TelemetryHelper.findIndex(entries, millis);
    return entries[index][indices.alt];
  }

  static latLonAt(entries, millis) {
    const index = millis <= 0 ? 0 : TelemetryHelper.findIndex(entries, millis);
    const latlon = entries[index].slice(0, 2);
    return latlon;
  }

  static timeSeriesAltitude(entries) {
    return entries
      .filter((o) => o[indices.millis] > 0)
      .filter((o) => Number.isFinite(o[indices.alt]))
      .map((o) => [o[indices.millis], o[indices.alt]]);
  }
}
