import Link from "next/link";
import Icon from "./Icon";

export default function() {
  return (
    <header>
      <div className="header-content">
        <a href="/">
          <Icon figure="markerHeader" className="header" />
        </a>
        <p>
          <em>Hikimaps</em>
          <br />
          Routes for active people
        </p>
      </div>
      <style jsx global>{`
        header {
          width: 100%;
          height: 4em;
          background: #1bb654;
        }
        .header-content {
          width: 100%;
          max-width: 640px;
          margin: 0 auto;
          box-sizing: border-box;
          height: 4em;
          position: relative;
        }
        header .hiki-icon {
          position: absolute;
          top: 5px;
          left: 5px;
        }
        header p {
          position: absolute;
          top: 8px;
          left: 1.6cm;
          margin: 0;
          color: #fff;
        }
        header p em {
          font-weight: bold;
          font-size: 1rem;
          font-style: normal;
        }
      `}</style>
    </header>
  );
}
