import React, { Component } from "react";
import Icon from "./Icon";
import Distance from "./Distance";

export default function RouteListImageLink({ entry }) {
  return (
    <li>
      <a href={`/${entry.slug}`}>
        <div className="image-container">
          <img src={`https://img.youtube.com/vi/${entry.youtubeId}/0.jpg`} />
          <span className="icons-wrapper">
            {entry.activities &&
              entry.activities.map((activity, index) => (
                <Icon key={index} figure={activity} className="index" />
              ))}
          </span>
        </div>
        <div className="text-container">
          {entry.title}
          <Distance
            kilometers={entry.kilometers}
            style={{
              position: "absolute",
              width: "4em",
              textAlign: "center",
              display: "inline-block",
              top: 3,
              left: 3,
              background: "#fff",
              border: "1px solid white"
            }}
          />
        </div>
      </a>
      <style jsx>{`
        li {
          margin: 0 10px 5em 0;
          line-height: 1.5em;
          height: 240px;
          width: calc(50% - 20px);
          display: inline-block;
          box-sizing: border-box;
          float: left;
        }
        .image-container {
          height: 200px;
          width: 100%;
          position: relative;
          overflow: hidden;
          box-sizing: border-box;
        }
        img {
          width: 140%;
          position: absolute;
          left: -20%;
          top: -45px;
        }
        .text-container {
          padding: 1em 0.3em 1.5em 0.2em;
          height: 100px;
          box-sizing: border-box;
        }
        a,
        a:visited,
        a:active {
          display: block;
          line-height: 1.5em;
          width: 100%;
          height: 100%;
          color: #38b61b;
          text-decoration: none;
          position: relative;
        }
        a:hover {
          text-decoration: underline;
        }
        .icons-wrapper {
          position: absolute;
          right: 3px;
          bottom: 3px;
        }
        @media screen and (max-width: 480px) {
          li {
            width: calc(100% - 10px);
          }
          a {
            padding-bottom: 2.6em;
            margin-bottom: 1em;
          }
        }
      `}</style>
    </li>
  );
}
