import React, { Component } from "react";
import OnboardingPolylineClick from "../components/OnboardingPolylineClick.js";
import Icon from "./Icon";

let L = {};
let ReactLeaflet = {};

export default class LazyMap extends Component {
  constructor(props) {
    super(props);

    this.handleMapViewportChange = this.handleMapViewportChange.bind(this);
    this.handleMapClickPolyline = this.handleMapClickPolyline.bind(this);
    this.handleMapClick = this.handleMapClick.bind(this);
    this.handleMapReady = this.handleMapReady.bind(this);

    this.state = {
      telemetry: this.props.telemetry,
      mounted: false,
      tile: 0,
      viewport: {}
    };
  }

  async componentDidMount() {
    ReactLeaflet = await import("react-leaflet");
    L = await import("leaflet");
    L.Icon.Default.imagePath =
      "//cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/images/";
    const points = this.state.telemetry.map(([lat, lon]) => [lat, lon]);
    const marker = points[0].slice();
    const polyline = L.polyline(points);
    const bounds = polyline.getBounds();
    const center = points[0].slice();
    const icon = L.divIcon({
      className: "hiki-position-marker",
      html: Icon.svgs.markerMap,
      iconAnchor: [16, 30],
      iconSize: [32, 32]
    });
    this.setState({ mounted: true, bounds, center, points, marker, icon });
  }

  componentDidUpdate(prevProps) {
    if (this.props.marker != prevProps.marker) {
      this.setState({ marker: this.props.marker });
    }
  }

  handleMapReady(ev) {
    console.log("handleMapReady", ev);
    ev.target.fitBounds(this.state.bounds, { padding: [10, 10] });
  }

  handleMapViewportChange(viewport) {
    this.setState({ viewport });
    this.props.onMapViewportChange && this.props.onMapViewportChange(viewport);
  }

  handleMapClick(ev) {
    console.log("handleMapClick", ev);
    this.props.onMapClick && this.props.onMapClick(ev);
  }

  handleMapClickPolyline(ev) {
    console.log("handleClickPolyline", ev);
    const point = ev.latlng;
    const { index } = this.state.points
      .map((p, index) => {
        return {
          index,
          distance: Math.sqrt(
            Math.pow(point.lat - p[0], 2) + Math.pow(point.lng - p[1], 2)
          )
        };
      })
      .sort((a, b) => (a.distance > b.distance ? 1 : -1))
      .shift();
    const marker = this.state.points[index];
    this.setState({ marker });

    const offsetMilliSeconds = this.state.telemetry[index][2];
    const offsetSeconds = offsetMilliSeconds / 1000;
    this.props.onClickPolyline && this.props.onClickPolyline(offsetSeconds);
  }

  render() {
    const { mounted, tile, center, marker, icon } = this.state;
    const { Map, TileLayer, Polyline, Marker } = ReactLeaflet;
    return (
      <div className="map-wrapper">
        <OnboardingPolylineClick />
        {mounted && (
          <Map
            whenReady={this.handleMapReady}
            viewport={this.state.viewport}
            onViewportChanged={this.handleMapViewportChange}
            onClick={this.handleMapClick}
            center={center}
            zoom={11}
          >
            {tile === 0 && (
              <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              />
            )}
            {tile === 2 && (
              <TileLayer url="https://{s}.api.mapbox.com/styles/v1/mapbox/outdoors-v10/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiaGlpdHN1IiwiYSI6ImNpd3VzYzU3ZTAwMjgydHBleXYwdXRmNjMifQ.SAzN0XOBb00AS9Y3qX-NfA" />
            )}
            {tile === 1 && (
              <TileLayer url="https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png" />
            )}
            <Polyline
              onClick={this.handleMapClickPolyline}
              color="red"
              positions={this.state.points}
            />
            <Marker position={marker} icon={icon} />
          </Map>
        )}
        <style>{`
          .map-wrapper {
            position: relative;
            height: 330px;
          }
          .leaflet-container {
            height: 320px;
            margin-bottom: 10px;
          }
          @media screen and (max-width: 480px) {
            .map-wrapper {
              position: relative;
              height: 250px;
            }
            .leaflet-container {
              height: 240px;
              margin-bottom: 10px;
            }
          }
        `}</style>
      </div>
    );
  }
}
