export const GA_TRACKING_ID = "UA-85329234-1";

function isLocalhost() {
  return location.hostname === "localhost";
}

function isDev() {
  return process.env.NODE_ENV !== "production";
}

// https://developers.google.com/analytics/devguides/collection/gtagjs/pages
export const pageview = url => {
  if (isLocalhost() || isDev()) {
    console.log("record analytics pageview", url);
  }
  !isDev() &&
    window &&
    window.gtag("config", GA_TRACKING_ID, {
      page_path: url
    });
};

// https://developers.google.com/analytics/devguides/collection/gtagjs/events
export const event = ({ action, category, label, value }) => {
  if (isLocalhost() || isDev()) {
    console.log("record analytics event:", { action, category, label, value });
  }
  !isDev() &&
    window &&
    window.gtag("event", action, {
      event_category: category,
      event_label: label,
      value: value
    });
};
