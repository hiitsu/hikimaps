import React from "react";
import PropTypes from "prop-types";

import Link from "next/link";
import Cookies from "universal-cookie";

const cookies = new Cookies();

class CookieConsent extends React.Component {
  constructor(props) {
    super(props);

    this.handleApprove = this.handleApprove.bind(this);

    this.state = {
      show: false
    };
  }

  componentDidMount() {
    const show = !cookies.get("cookie-consent");
    this.setState({ show });
  }

  handleApprove() {
    this.setState({ show: false });
    cookies.set("cookie-consent", "1", { path: "/" });
  }

  render() {
    if (!this.state.show) return null;

    return (
      <div className="CookieConsent">
        <p>This site uses Google Analytics</p>
        <button onClick={this.handleApprove}>Got it</button>
        <style jsx global>{`
          .CookieConsent {
            position: fixed;
            top: 0;
            right: 0;
            width: 320px;
            height: 4em;
            text-align: right;
            padding: 0;
            background: black;
            z-index: 100001;
          }
          .CookieConsent p {
            width: 240px;
            height: 4em;
            padding: 0;
            margin: 0;
            line-height: 4em;
            white-space: nowrap;
            color: white;
          }
          .CookieConsent button {
            position: absolute;
            right: 12px;
            top: 11px;
            background: white;
            border: 0;
            color: black;
            padding: 8px 14px;
          }
          .CookieConsent a {
            color: white;
          }
        `}</style>
      </div>
    );
  }
}

export default CookieConsent;
