# Hikimaps

## Todo 10.05.2020

Small fixes:

- warn about negative timestamps while building
- show map already when it is loaded, before telemetry JSON arrives
- improve Elevantion chart performance/UX, its flickering if hovering and feels slowish

Content, answer to these questions:

- are there any alternatives ?
- parking ?
- cell tower coverage ?
- are the child friendly?
- dangers, can go to dangerous areas like shooting grounds or with wild animals?
- can you get lost ?
- best time of the year or if valid whole year ?
- mention most suitable sports and skill levels needed and tag if fishing is possible, day hike vs hiking over night, also if this is normally peaceful

in general things to do or think:

- add interesting cue points to videos
- points of interest, how ?
- automate youtube video descriptions, cue points and backlinks ?

Feats:

- ability to watch content in different order (stacked, minimapped, widescreen etc)
- showing clear variations of the same route as bundle "1/3" text or something next to it
- indicate if video or height data is available

## Status

[![Netlify Status](https://api.netlify.com/api/v1/badges/db10d4e3-9bb3-4954-9a35-f03a61fd83a5/deploy-status)](https://app.netlify.com/sites/hikimaps/deploys)
